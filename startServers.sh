#!/bin/ksh

git config --global user.name "Loizos Vasileiou"
git config --global user.email "loizosv@amdocs.com"


echo "Sttart Tomcat Server on port 8080"
nohup ~/Downloads/apache-tomacat-9.0.30/bin/startup.sh &

echo "Start Jenkins Server on port 9090"
nohup java -jar ~/Downloads/jenkins.war --httpPort=9090 &

echo "Start JFrog Server on port 8081"
nohup ~/Downloads/artifactory-oss-6.14.0/bin/artifactory.sh &
